# MIS Sample APP

<a name="To-Windows-APP-Candidate"></a>

# To Windows APP Candidate - MIS Team (Transcend)

Hi ,

Thanks for your interest in the position.  

We have a Windows APP exam (C#) before the interview. Please finish it and commit your code to GitLab before interview date. 

Please create a new branch for your commit. 
**You may need the permission to commit and create branch, please provide your GitLab account to us.**

<a name="Getting-Started"></a>

## Getting Started

- Please install Visual Studio Express first.  
[https://visualstudio.microsoft.com/zh-hant/vs/express/](https://visualstudio.microsoft.com/zh-hant/vs/express/)

- Please go GitLab to register an account and clone the following project.  
[https://gitlab.com/ts-candidate-windows/missampleapp](https://gitlab.com/ts-candidate-windows/missampleapp)

- Open the solution file: SampleApp.sln

In this APP, you have to finish the 3 functions :

<a name="1-ImportExcel"></a>

## 1\. Import Excele data to DataGridview  :

There is an Excel file (data.xlsx) in the folder. 
Please implement a function to OpenFileDialog and get the selected Excele file path in textbox then import the Excel Sheet1 data to the DataGridview.

**Expected Result :**  

**Tap "Open Excel" button > OpenFileDialog > Select "data.xlsx" > show the file path in textbox.**
![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/exam/misexam0.PNG)

**Tap "Import Excel" button > get the Excel Sheet1 data and fill data in the DataGridview.**
![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/exam/misexam1.PNG)

## 2\. Insert data to SQLite database  :

There is a SQLite db file (database.db) in the folder. 
Please implement a function to insert data from the DataGridview to SampleTable in SQLite database(database.db).

**Tap "Insert Data" button > get the DataGridview data and insert to SampleTable in database.**

## 3\. SQL Query  :
Please implement a  SQL Query function for SQLite database: 
 - get a table with (Office, Item ,OrderNo, Price, MSP) 
 - where the  value MSP > Price 
 - sort by MSP value 
 - update the result in the DataGridview.

**Tap "SQL Query" button > get the SQL Query result and Update in DataGridview.**
![](https://s3-ap-northeast-1.amazonaws.com/test.storejetcloud.com/exam/misexam4.PNG)





